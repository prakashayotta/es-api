package com.ayotta;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ayotta.model.XxmSpParams;
import com.ayotta.repositories.XxmSpParamsRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderJobApplicationTests {

	@Autowired
	XxmSpParamsRepository paramsRepo;
	List<XxmSpParams> paramList = new ArrayList<XxmSpParams>();
String mailFrom;
	
//	@Value("${spring.mail.to}")
	String mailTo;
	@Test
	public void contextLoads() {
		
		paramList =  paramsRepo.findByParameterNameLike("ES_MAIL");
		
		paramList.forEach(param -> {
			if(param.getParameterName().equals("ES_MAIL_FROM")) {
				mailFrom = param.getParameterValue();
			}
            if(param.getParameterName().equals("ES_MAIL_TO")) {
            	mailTo = param.getParameterValue();
			}
		});
	}

}
