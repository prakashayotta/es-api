package com.ayotta.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ElasticsearchConfig {

	Logger logger = LoggerFactory.getLogger(ElasticsearchConfig.class);
	
	@Value("${elasticsearch.host}")
    private String elasticsearchHost;
	
	@Value("${elasticsearch.port}")
    private int elasticsearchPort;

    @Bean(destroyMethod = "close")
    public RestHighLevelClient client() {

    	logger.info("el port: "+elasticsearchPort+" "+"el host: " +elasticsearchHost );
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(new HttpHost(elasticsearchHost,elasticsearchPort,"http")
                		));

        return client;

    }
	
	 @Bean(destroyMethod = "close")
   public RestClient client2() {

   	logger.info("el port: "+elasticsearchPort+" "+"el host: " +elasticsearchHost );
       RestClient client = RestClient.builder(new HttpHost(elasticsearchHost,elasticsearchPort,"http")).build();

       return client;

   }
}
