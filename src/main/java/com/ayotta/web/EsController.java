package com.ayotta.web;

import javax.validation.constraints.Max;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ayotta.model.EDIOrder;
import com.ayotta.model.EDIOrderHeader;
import com.ayotta.services.EdiService;
import com.mbd.feeds.model.ETAFeed;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/es/orders")
@Api(value = "EDI order Management", description = "Operations with elasticsearch orders", produces = "application/json")
public class EsController {
	
	private static final Logger logger = LoggerFactory.getLogger(EsController.class);

	@Autowired
	EdiService service;
	
	@PostMapping("/processByStatus")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Process orders from elastic search based on status")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successful Operation"), 
			@ApiResponse(code = 400, message = "Invalid Order Header ID Supplied"),
			@ApiResponse(code = 404, message = "Order Not Found")
			})
	public void processToDB(@RequestParam(name="esRefId") String esRefId, 
			@RequestParam(name="orderSource") String orderSource, 
			@ApiParam(value ="Please enter the messageStatus of an order.(Example:SAVE, PROCESS)") @RequestParam(name="messageStatus") String messageStatus) {
		logger.debug("elasticSearchRef :"+ esRefId +"orderSource : "+orderSource+"messageStatus :"+messageStatus);
		service.processToDB(esRefId, orderSource, messageStatus, null, null);
	}
	
	@PostMapping("/processByDate")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Process orders from elastic search based on date")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successful Operation"), 
			@ApiResponse(code = 400, message = "Invalid Order Header ID Supplied"),
			@ApiResponse(code = 404, message = "Order Not Found")
			})
	public void processRecordToDB(
			@RequestParam(name="orderSource") String orderSource, 
			@ApiParam(value ="Please enter the from date in the format DD-MMM-YYYY.(Example:01-Jan-2020)") 
			@RequestParam(required = false,name="fromDate") String fromDate,
			@ApiParam(value ="Please enter the to date in the format DD-MMM-YYYY.(Example:02-Jan-2020)") 
			@RequestParam(required = false,name="toDate") String toDate) {
		logger.debug("fromDate : "+fromDate+"toDate :"+toDate);
		service.processToDB(null, orderSource, "READ_FROM_FILE", fromDate, toDate);
	}
	
	@GetMapping("/")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get list of order in the System")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successful Operation"), 
			@ApiResponse(code = 400, message = "Invalid Order Header ID Supplied"),
			@ApiResponse(code = 404, message = "Order Not Found")
			})
	public EDIOrder fetchOrderByStatus(
			@ApiParam(value ="Please enter the order source of an order.(Example:edi)") @RequestParam(required = true, name="orderSource") String orderSource,
			@ApiParam(value ="Please enter the messageStatus of an order.(Example:READ_FROM_FILE, SAVED_IN_DB, PROCESS_FAILED, PROCESSED)") @RequestParam(required = false,name="messageStatus") String messageStatus, 
			@ApiParam(value ="Please enter the from date in the format DD-MMM-YYYY.(Example:01-Jan-2020)") @RequestParam(required = false,name="fromDate") String fromDate,
			@ApiParam(value ="Please enter the to date in the format DD-MMM-YYYY.(Example:02-Jan-2020)") @RequestParam(required = false,name="toDate") String toDate,
			@ApiParam(value ="Please enter the page number.(Example:1)") @RequestParam(required = true,name="from") @Max(1) Integer fromNo) {
		EDIOrder listOfEDIOrder = service.fetchOrderByStatus(orderSource,messageStatus,fromDate,toDate,fromNo,false);
		return listOfEDIOrder;
	}
	
	
	@DeleteMapping("/delete")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Delete a entry from elasticsearch")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successful Operation"), 
			@ApiResponse(code = 400, message = "Invalid Order Header ID Supplied"),
			@ApiResponse(code = 404, message = "Order Not Found")
			})
	public void fetchOrderByStatus(
			@RequestParam(required = true, name="orderSource") String orderSource,
			@RequestParam("elasticSearchRef") String elasticSearchRef) {
		try {
		if(orderSource.equalsIgnoreCase("edi")) {
			EDIOrderHeader doc = new EDIOrderHeader();
			doc.setElasticSearchRef(elasticSearchRef);
			doc.setOrderSource(orderSource);
			String retVal = service.deleteOrder(doc);
		}else {
			ETAFeed doc = new ETAFeed();
			doc.setElasticSearchRef(elasticSearchRef);
			doc.setOrderSource(orderSource);
			String retVal =  service.deleteFeedsOrder(doc);
		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@PostMapping("/update/{refId}")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Update an order in elasticsearch")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successful Operation"), 
			@ApiResponse(code = 400, message = "Invalid Order Header ID Supplied"),
			@ApiResponse(code = 404, message = "Order Not Found")
			})
	public ResponseEntity<EDIOrderHeader> updateWebOrderLine(
	
		 @PathVariable("refId") String refId)  {
		
		EDIOrderHeader orderHder = service.fetchOrder(refId);
			
		return new ResponseEntity<EDIOrderHeader>(HttpStatus.OK);
	}
	
}
