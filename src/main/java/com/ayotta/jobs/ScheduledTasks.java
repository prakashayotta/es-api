package com.ayotta.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ayotta.config.YamlConfig;
import com.ayotta.services.EdiService;


@Component
public class ScheduledTasks {
	private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
	
	@Autowired
	EdiService ediService;
	
	@Autowired
	YamlConfig properties;
	
//	@Scheduled(cron = "0 0/2 * * * *") //EVERY 2mins CALL
	@Scheduled(cron = "0 0 * * * *") //EVERY HOURS CALL
	public void scheduleES() {
		 try {
			 logger.debug("scheduleES");
			 String orderSrcStr = properties.getOrderSource();
			 String[] orderSource = orderSrcStr.split(",");
			 for(String orderSrc : orderSource) {
				 ediService.fetchOrderByStatus(orderSrc,null,null,null,null,true);
			 }
//			 ediService.sendMsgStatusReport();
		} catch (Exception e) {
			logger.debug("ERROR : "+ e.getMessage());
			e.printStackTrace();
		}
	}
}
