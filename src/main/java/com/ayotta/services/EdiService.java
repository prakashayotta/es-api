package com.ayotta.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.security.auth.login.Configuration;
import javax.transaction.Transactional;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.HttpAsyncResponseConsumerFactory;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.CountRequest;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.ayotta.config.YamlConfig;
import com.ayotta.kafka.Producer;
import com.ayotta.model.EDIOrder;
import com.ayotta.model.EDIOrderHeader;
import com.ayotta.model.EDIOrderLine;
import com.ayotta.model.MailProperties;
import com.ayotta.model.XxmSpParams;
import com.ayotta.model.XxmSpiceActions;
import com.ayotta.repositories.EDIOrderHeaderRepository;
import com.ayotta.repositories.EDIOrderLineRepository;
import com.ayotta.repositories.XxmSpParamsRepository;
import com.ayotta.repositories.XxmSpiceActionsRepo;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mbd.feeds.model.ETAFeed;

@Service
@Transactional
public class EdiService {
	
	@Autowired
	EDIOrderHeaderRepository headerRepository;

	@Autowired
	EDIOrderLineRepository orderLineRepository;

	private RestHighLevelClient esClient;
	
	private RestClient restClient;

	@Autowired
	private ObjectMapper objectMapper;

	private Logger logger = LoggerFactory.getLogger(EdiService.class);

	private static String INDEX = "orders";

	private static String TYPE = "order";
	
	private static String FEEDS_INDEX = "feeds";

	private static String FEEDS_TYPE = "feed";
	
	@Autowired
    private JavaMailSender emailSender;
	
//	@Value("${spring.mail.from}")
	String mailFrom;
	
//	@Value("${spring.mail.to}")
	String mailTo;

	private static String mailSub = "Orders in ElasticSearch";
	
	private static final String msgStatus = "READ_FROM_FILE";
	
	@Autowired
	YamlConfig properties;
	
	@Autowired
	public EdiService( RestHighLevelClient esClient) {

		this.esClient = esClient;
	}
	
	@Autowired
	XxmSpParamsRepository paramsRepo;
	
	@Autowired
	EntityManager em;
	
	@Autowired
	XxmSpiceActionsRepo xxmSpiceActionsRepo;
	
	@Autowired
	MailProperties mailProperties;
	
	List<XxmSpParams> paramList = new ArrayList<XxmSpParams>();
	
	final Scroll scroll = new Scroll(TimeValue.timeValueMinutes(1L));
	ObjectMapper jacksonMapper = new ObjectMapper();
	
	@Autowired
	Producer producer;
//	public EdiService ( Producer producer) {
//		this.producer = producer;
//	}
	
	static Map<String, String> searchFields = new HashMap<String, String>() ;
	
	static {
//		searchFields.put("PROCESS", "SAVED_IN_DB");
		searchFields.put("SAVE", "READ_FROM_FILE");
	}
	
	@PostConstruct
	public void loadParamValue() {
		
		paramList =  paramsRepo.findByParameterNameLike("ES_MAIL%");
		logger.debug("Enter the loadParamValue : "+ paramList.toString());
		paramList.forEach(param -> {
			if(param.getParameterName().equals("ES_MAIL_FROM")) {
				mailFrom = param.getParameterValue();
			}
            if(param.getParameterName().equals("ES_MAIL_TO")) {
            	mailTo = param.getParameterValue();
			}
		});
	
	}
	
	public EDIOrderHeader saveInDB(String message) throws JsonParseException, JsonMappingException, IOException {

		EDIOrderHeader orderHeader = convertStringToOrder(message);

		orderHeader.setCreationDate(new Date());
		orderHeader.setMessageStatus("SAVED_IN_DB");
		orderHeader = headerRepository.save(orderHeader);
		logger.info("Order header Id : "+ orderHeader.getOrderHeaderId());
		for (EDIOrderLine orderLine: orderHeader.getOrderLines()) {
			orderLine.setOrderHeaderId(orderHeader.getOrderHeaderId());
			orderLineRepository.save(orderLine);
		}
		return orderHeader;
	}

	public String updateOrder(EDIOrderHeader document) throws Exception {
		EDIOrderHeader resultDocument = this.findById(document.getElasticSearchRef());
		UpdateRequest updateRequest = new UpdateRequest(INDEX, TYPE, resultDocument.getElasticSearchRef());
		updateRequest.doc(convertOrderToMap(document));
		UpdateResponse updateResponse = esClient.update(updateRequest, RequestOptions.DEFAULT);
		logger.debug("updateResponse : "+ updateResponse.getResult());
		return updateResponse.getResult().name();
	}
	
	public String deleteOrder(EDIOrderHeader document) throws Exception {
		EDIOrderHeader resultDocument = this.findById(document.getElasticSearchRef());
		DeleteRequest deleteRequest = new DeleteRequest(INDEX,TYPE,resultDocument.getElasticSearchRef());
		DeleteResponse deleteResponse = esClient.delete(deleteRequest, RequestOptions.DEFAULT);
		logger.debug("deleteResponse : "+ deleteResponse.getResult());
		return deleteResponse.getResult().name();
	}
	
	public String deleteFeedsOrder(ETAFeed document) throws Exception {
		ETAFeed resultDocument = this.findByIdFeeds(document.getElasticSearchRef());
		DeleteRequest deleteRequest = new DeleteRequest(FEEDS_INDEX, FEEDS_TYPE, resultDocument.getElasticSearchRef());
		DeleteResponse response = esClient.delete(deleteRequest, RequestOptions.DEFAULT);
		return response.getResult().name();
	}
	
	public ETAFeed findByIdFeeds(String id) throws Exception {
		GetRequest getRequest = new GetRequest(FEEDS_INDEX, FEEDS_TYPE, id);
		GetResponse getResponse = esClient.get(getRequest, RequestOptions.DEFAULT);
		Map<String, Object> resultMap = getResponse.getSource();
		return convertMapToFeedsOrder(resultMap);
	}
	
	private ETAFeed convertMapToFeedsOrder(Map<String, Object> map) {
		return objectMapper.convertValue(map, ETAFeed.class);
	}

	public EDIOrderHeader findById(String id) throws Exception {
		GetRequest getRequest = new GetRequest(INDEX, TYPE, id);
		GetResponse getResponse = esClient.get(getRequest, RequestOptions.DEFAULT);
		Map<String, Object> resultMap = getResponse.getSource();
		return convertMapToOrder(resultMap);
	}
	
	public void processToDB(String refId, String orderSource, String messageStatus, String fromDate, String toDate) {
		OffsetDateTime instant = OffsetDateTime.now();
		OffsetDateTime instantHourEarlier = OffsetDateTime.now().minusHours(1);
		LocalDate fDate = null;
		LocalDate tDate = null;
		logger.debug("Enter the processToDB : "+ "to : "+ instant +"from : "+instantHourEarlier);
		try {
			SearchRequest searchRequest = new SearchRequest();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
			if(orderSource.equals("edi")){
				searchFields.put("PROCESS", "SAVED_IN_DB");
				searchRequest.indices(INDEX);
				searchRequest.types(TYPE);
			}else{
				searchFields.put("PROCESS", "PROCESS_FAILED");
				searchRequest.indices(FEEDS_INDEX);
				searchRequest.types(FEEDS_TYPE);
			}
            if(fromDate != null && !fromDate.equals("")) {
				fDate = LocalDate.parse(fromDate, formatter);
			}
			if(toDate != null && !toDate.equals("")) { 
				tDate = LocalDate.parse(toDate, formatter);
			}
			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//			MatchQueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("elasticSearchRef", id).operator(Operator.AND);
			BoolQueryBuilder qb = QueryBuilders.boolQuery();
			if(refId != null && refId.indexOf(",") != -1) {
				String[] esRefId = refId.split(",");
//				List<String> esRefIdList = Arrays.stream(esRefId).collect(Collectors.toList());
//				qb.must(QueryBuilders.termsQuery("_id", "898542a7-8a6f-437a-bdbc-c178ca44037e","bbac22f4-e993-4836-beeb-7990d34ae319"));
				qb.must(QueryBuilders.termsQuery("_id", esRefId));
			}else {
				if(refId != null) {
					qb.must(QueryBuilders.matchQuery("elasticSearchRef", refId));
				}
			}
			
			if(messageStatus != null && !messageStatus.equals("")) {
				qb.must(QueryBuilders.matchQuery("messageStatus", searchFields.get(messageStatus.toUpperCase())));
			}
			
			if(fDate != null) {
				qb.must(QueryBuilders.rangeQuery("creationDate").gte(fDate));
			}
			if(tDate != null) {
				qb.must(QueryBuilders.rangeQuery("creationDate").lte(tDate));
			}
			searchSourceBuilder.query(qb);
			searchRequest.source(searchSourceBuilder);
			SearchResponse searchResponse = esClient.search(searchRequest, RequestOptions.DEFAULT);
			if(orderSource.equalsIgnoreCase("edi")) {
				List<EDIOrderHeader> listOfEDIOrder = getSearchResult(searchResponse);
				if(listOfEDIOrder != null && listOfEDIOrder.size() > 0) {
					logger.debug("LIST SIZE EDI ORDER:"+listOfEDIOrder.size());
					listOfEDIOrder.forEach(order -> {
						try {
							String objectString = jacksonMapper.writeValueAsString(order);
							if((messageStatus != null && messageStatus.toUpperCase().equals("PROCESS")) && 
									order.getMessageStatus().equals("SAVED_IN_DB")) {
								producer.sendMessage("edi_procedure_call",objectString);
								logger.info(" ORDER: "+ order.getCustPoNo() + " sent to message queue");
							}else if((messageStatus != null && messageStatus.toUpperCase().equals("SAVE")) &&
									order.getMessageStatus().equals("READ_FROM_FILE")){
								producer.sendMessage("edi",objectString);
								logger.info(" ORDER: "+ order.getCustPoNo() + " sent to message queue");
							}
						}catch (Exception e) {
							e.printStackTrace();
						}
					});
				}else {
					logger.debug("LIST SIZE EDI ORDER:"+listOfEDIOrder.size());
				}
			}else if(orderSource.equalsIgnoreCase("feeds")) {
				List<ETAFeed> etaFeeds = getFeedsSearchResult(searchResponse);
				if(etaFeeds != null) {
					logger.debug("LIST SIZE FEEDS ORDER:"+etaFeeds.size());
					etaFeeds.forEach(order -> {
						try {
							String objectString = jacksonMapper.writeValueAsString(order);
							if((messageStatus != null && messageStatus.toUpperCase().equals("PROCESS")) &&
									order.getMessageStatus().equals("PROCESS_FAILED")) {
								producer.sendMessage("feeds_procedure_call",objectString);
							}else if((messageStatus != null && messageStatus.toUpperCase().equals("SAVE")) &&
									order.getMessageStatus().equals("READ_FROM_FILE")){
								producer.sendMessage("feeds",objectString);
								logger.info("FEED FILE  "+order.getFileName()+ " ORDER: "+ order.getElasticSearchRef() + " sent to message queue");
							}
						}catch (Exception e) {
							e.printStackTrace();
						}
					});
				}else {
					logger.debug("LIST SIZE FEEDS ORDER:"+etaFeeds.size());
				}
			}else {
				logger.debug("ORDER SOURCE :"+orderSource);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private EDIOrderHeader saveOrderInDB(EDIOrderHeader orderHeader) {
		orderHeader.setCreationDate(new Date());
		orderHeader.setMessageStatus("SAVED_IN_DB");
		orderHeader = headerRepository.save(orderHeader);
		logger.info("Order header Id : "+ orderHeader.getOrderHeaderId());
		for (EDIOrderLine orderLine: orderHeader.getOrderLines()) {
			orderLine.setOrderHeaderId(orderHeader.getOrderHeaderId());
			orderLineRepository.save(orderLine);
		}
		
		try {
			logger.info(updateOrder(orderHeader));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(orderHeader.getOrderHeaderId() >0) {
			orderHeader.setMessageStatus("PROCESSED");
			processEdiOrders(orderHeader.getOrderHeaderId(),orderHeader);
		}
		
		logger.debug("Processed EDI Order " + orderHeader.getOrderHeaderId());
		
		return orderHeader;
	}
	
	public int processEdiOrders(Long headerId, EDIOrderHeader orderHeader) {
		Integer retVal=0;
//		parameters.put("P_ERPSOURCE","SAP");
//		parameters.put("P_HEADER_ID",headerId);
//		
//		logger.debug("ETA.XMP_PROCESS_EDI_ORDERS.process_mdsparesorder_in_edi("+parameters);
//		retVal = getNamedParameterJdbcTemplate()
//		.update("call ETA.XMP_PROCESS_EDI_ORDERS.process_mdsparesorder_in_edi( " +
//				  ":P_ERPSOURCE"+
//		          ",:P_HEADER_ID)",
//				parameters);
		/*Session session = getSession();
    	session.doWork(new Work() {
			@Override
			public void execute(Connection connection) throws SQLException {
				// TODO Auto-generated method stub
				CallableStatement cstmt = connection
						.prepareCall("CALL ETA.XMP_PROCESS_EDI_ORDERS.process_mdsparesorder_in_edi(?,?)");
				cstmt.setString(1, "SAP");
				cstmt.setLong(2, headerId);
				cstmt.execute();
				cstmt.close();
			}
    	});*/
		String source = "SAP";
		logger.info("process ediProcCall...");
			em.createNativeQuery("CALL ETA.XMP_PROCESS_EDI_ORDERS.process_mdsparesorder_in_edi(:source,:headerId)")
	        .setParameter("source", source).setParameter("headerId", headerId).executeUpdate();
	    logger.debug("processed feedsProcCall...");
		boolean aogFlag= false;
		XxmSpiceActions action = new XxmSpiceActions();
		action.setExoHdrid(headerId);
		List<XxmSpiceActions> list = xxmSpiceActionsRepo.findByExoHdrid(action.getExoHdrid());
		if(list!=null && list.size()>0){
			for(XxmSpiceActions object:list){
				if(object.getAogFlag()!=null && object.getAogFlag().equals("Y")){
					aogFlag=true;
				}
			}
		}
		if(aogFlag){
			processAOG(orderHeader);
		}
		
		return retVal;
	}

	public void processAOG(EDIOrderHeader orderHeader){
		MimeMessage message = emailSender.createMimeMessage();
    	try {
    		logger.debug("MailFrom: "+mailProperties.getMailFrom());
    		
    		MimeMessageHelper helper;
    		
    		helper = new MimeMessageHelper(message, true);
    		
    		helper.setFrom(mailProperties.getMailFrom());
    		if(mailProperties.getMailAogTo()!=null && !mailProperties.getMailAogTo().equals("")) {
    			
    			helper.setTo(mailProperties.getMailAogTo());
    		}else {
    			helper.setTo(mailProperties.getSchemaMailTo());
    		}
    		helper.setSubject("B2B MESSAGE: "
    				+ "WEB"
    				+ " INBOUND AOG MESSAGE @"
    				+ new SimpleDateFormat("dd-MM-yyyy hh:mm:ss")
    				.format(new Date()));
    		helper.setText("The following AOG Message has arrived and will be sent to Oracle.\n"
    				+ orderHeader.toString() + "", false);
    		
    		emailSender.send(message);
    		logger.info("SENT_AOG_MAIL TO \t" + mailProperties.getMailAogTo() + ": ");
    		logger.info("Mail has been sent successfully...");
    	}catch (MailException exception) {
            exception.printStackTrace();
        } catch (MessagingException e) {
			e.printStackTrace();
		}
	}
//	public List<EDIOrderHeader> fetchOrderByStatus(String ordSource,String msgStatus,String fromDate, String toDate){
//		OffsetDateTime instant = null;
//		OffsetDateTime instantHourEarlier = null;
//		String orderSource = ordSource;
//		String messageStatus = msgStatus;
//		List<EDIOrderHeader> listOfEDIOrder = new ArrayList<EDIOrderHeader>();
//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
//		LocalDate fDate = LocalDate.parse(fromDate, formatter);
//		LocalDate tDate = LocalDate.parse(toDate, formatter);
//		if((fDate!= null && fDate.equals(LocalDate.now())) && (tDate!= null && tDate.equals(LocalDate.now()))) {
//			LocalTime midnight = LocalTime.MIDNIGHT;
//			instant = OffsetDateTime.now();
//			instantHourEarlier = OffsetDateTime.now().with(midnight);
//		}
//		
//		System.out.println("date :"+fDate +" "+ tDate);
//		try {
//			SearchRequest searchRequest = new SearchRequest();
//			searchRequest.indices(INDEX);
//			searchRequest.types(TYPE);
//			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
////			MatchQueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("messageStatus", messageStatusStr).operator(Operator.AND);
////			QueryBuilder matchDocumentsWithinRange = QueryBuilders.rangeQuery("creationDate").from(frmDate).to(nowDate);
//			 
//				QueryBuilder matchDocumentsWithinRange = QueryBuilders.boolQuery().must(QueryBuilders.rangeQuery("creationDate").gte(fDate)  
//						.lte(tDate))
//						.filter(QueryBuilders.matchQuery("messageStatus", messageStatus).operator(Operator.AND))
//						.filter(QueryBuilders.matchQuery("orderSource", orderSource).operator(Operator.AND));
//				        searchSourceBuilder.query(matchDocumentsWithinRange);
//			
//			searchRequest.source(searchSourceBuilder);
//			SearchResponse searchResponse = esClient.search(searchRequest, RequestOptions.DEFAULT);
//			listOfEDIOrder = getSearchResult(searchResponse);
//			logger.debug("LIST SIZE OF READ_FROM_FILE :"+listOfEDIOrder.size());
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return listOfEDIOrder;
//	}

	public EDIOrder fetchOrderByStatus(String orderSource,String messageStatus,String fromDate, String toDate, Integer fromNo,boolean isSchedular){
		OffsetDateTime instant = null;
		OffsetDateTime instantHourEarlier = null;
		List<EDIOrderHeader> listOfEDIOrder = new ArrayList<EDIOrderHeader>();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		LocalDate fDate = null;
		LocalDate tDate = null;
		EDIOrder ediOrder = new EDIOrder();
		
		if(isSchedular) {
			instant = OffsetDateTime.now();
			instantHourEarlier = OffsetDateTime.now().minusHours(1);
			messageStatus = msgStatus;
		}else {
			if(fromDate != null && !fromDate.equals("")) {
				fDate = LocalDate.parse(fromDate, formatter);
			}
			if(toDate != null && !toDate.equals("")) { 
				tDate = LocalDate.parse(toDate, formatter);
			}
		}
		
		try {
			SearchRequest searchRequest = new SearchRequest();
			if(orderSource.equalsIgnoreCase("edi")) {
				searchRequest.indices(INDEX);
				searchRequest.types(TYPE);
				
			}else {
				searchRequest.indices(FEEDS_INDEX);
				searchRequest.types(FEEDS_TYPE);
			}
			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			
			BoolQueryBuilder bqb = QueryBuilders.boolQuery(); 
			if (orderSource != null && !orderSource.equals("")) { 
				bqb.must(QueryBuilders.matchQuery("orderSource", orderSource));
			} 
			if(messageStatus != null && !messageStatus.equals("")) {
				bqb.must(QueryBuilders.matchQuery("messageStatus", messageStatus));
			}
			if(isSchedular) {
				bqb.must(QueryBuilders.rangeQuery("creationDate").gte(instantHourEarlier).lte(instant));
			}else {
				if(fDate != null) {
					bqb.must(QueryBuilders.rangeQuery("creationDate").gte(fDate));
				}
				if(tDate != null) {
					bqb.must(QueryBuilders.rangeQuery("creationDate").lte(tDate));
				}
			}
			
			if(fromNo!= null && !fromNo.equals("")) {
				fromNo--;
				int totalElements = 10;
				int  offset = totalElements * fromNo;
				searchSourceBuilder.query(bqb).from(offset);
			}else {
				searchSourceBuilder.query(bqb);
			}
			searchSourceBuilder.sort("creationDate", SortOrder.DESC);
			
			searchRequest.source(searchSourceBuilder);

			SearchResponse searchResponse = esClient.search(searchRequest,RequestOptions.DEFAULT);
			listOfEDIOrder = getSearchResult(searchResponse);
			logger.debug("LIST SIZE OF READ_FROM_FILE :"+listOfEDIOrder.size());
			if(listOfEDIOrder != null && listOfEDIOrder.size() > 0 && isSchedular) {
				try {
		        	MimeMessage message = emailSender.createMimeMessage();
		        	logger.debug("MailFrom: "+mailFrom);
		        	MimeMessageHelper helper;
		        	
		        	helper = new MimeMessageHelper(message, true);
		        	
					helper.setFrom(mailFrom);
					helper.setTo(mailTo);
					helper.setSubject(mailSub);
					helper.setText(listOfEDIOrder.toString());
		        	
		            emailSender.send(message);
		            logger.info("Mail has been sent successfully...");
		            return null;
		        } catch (MailException exception) {
		            exception.printStackTrace();
		        } catch (MessagingException e) {
					e.printStackTrace();
				}
				
			}
			
			ediOrder.setEdiOrders(listOfEDIOrder);
			
			SearchRequest request = new SearchRequest();
			searchRequest.indices(INDEX);
			searchRequest.types(TYPE);
			SearchSourceBuilder searchSourceBlder = new SearchSourceBuilder();
			
			BoolQueryBuilder bq = QueryBuilders.boolQuery(); 
			if (orderSource != null && !orderSource.equals("")) { 
				bq.must(QueryBuilders.matchQuery("orderSource", orderSource));
			} 
			if(messageStatus != null && !messageStatus.equals("")) {
				bq.must(QueryBuilders.matchQuery("messageStatus", messageStatus));
			}
			if(isSchedular) {
				bq.must(QueryBuilders.rangeQuery("creationDate").gte(instantHourEarlier).lte(instant));
			}else {
				if(fDate != null) {
					bq.must(QueryBuilders.rangeQuery("creationDate").gte(fDate));
				}
				if(tDate != null) {
					bq.must(QueryBuilders.rangeQuery("creationDate").lte(tDate));
				}
			}
			
			
			searchSourceBlder.query(bqb);
//			searchSourceBuilder.sort("creationDate", SortOrder.DESC);
			request.source(searchSourceBlder);
			request.scroll(scroll);
			SearchResponse response = esClient.search(request,RequestOptions.DEFAULT);
			listOfEDIOrder = getSizeOfSearchResult(response);
			ediOrder.setTotalSearchRecord(listOfEDIOrder.size());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return ediOrder;
	}
	
	private List<EDIOrderHeader> getSearchResult(SearchResponse response) {
		List<EDIOrderHeader> Orders = new ArrayList<>();
		String id = response.getScrollId();
		SearchHit[] searchHit = response.getHits().getHits();
		
		for (SearchHit hit : searchHit) {
			Orders.add(objectMapper.convertValue(hit.getSourceAsMap(), EDIOrderHeader.class));
		}
		return Orders;
	}
	
	private List<ETAFeed> getFeedsSearchResult(SearchResponse response) {
		SearchHit[] searchHit = response.getHits().getHits();
		List<ETAFeed> Orders = new ArrayList<>();
		for (SearchHit hit : searchHit) {
			Orders.add(objectMapper.convertValue(hit.getSourceAsMap(), ETAFeed.class));
		}
		return Orders;
	}
	
	private List<EDIOrderHeader> getSizeOfSearchResult(SearchResponse response) {
		List<EDIOrderHeader> Orders = new ArrayList<>();
		String id = response.getScrollId();
		SearchHit[] searchHit = response.getHits().getHits();
		
		while(searchHit != null && searchHit.length > 0) {
			SearchScrollRequest scrollRequest = new SearchScrollRequest(id); 
		    scrollRequest.scroll(scroll);
		    try {
				response = esClient.scroll(scrollRequest, RequestOptions.DEFAULT);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("erroe");
				e.printStackTrace();
			}
		    id = response.getScrollId();
		    searchHit = response.getHits().getHits();
		    
		    for (SearchHit hit : searchHit) {
				Orders.add(objectMapper.convertValue(hit.getSourceAsMap(), EDIOrderHeader.class));
			}
		}
	
		return Orders;
	}

	private Session getSession() {
        // get session from entitymanager. Assuming hibernate
        return em.unwrap(org.hibernate.Session.class);
    }
	
	private Map<String, Object> convertOrderToMap(EDIOrderHeader Order) {
		return objectMapper.convertValue(Order, Map.class);
	}

	private EDIOrderHeader convertMapToOrder(Map<String, Object> map) {
		return objectMapper.convertValue(map, EDIOrderHeader.class);
	}

	private EDIOrderHeader convertStringToOrder(String map)
			throws JsonParseException, JsonMappingException, IOException {
		return objectMapper.readValue(map, EDIOrderHeader.class);
	}
	
	public EDIOrderHeader fetchOrder(String refId) {
		EDIOrderHeader orderHeader = headerRepository.findByElasticSearchRef(refId);
		logger.debug("ORDERLINE SIZE :"+ orderHeader.getOrderLines().size());
		try {
			updateOrder(orderHeader);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return orderHeader;
	
	}

}
