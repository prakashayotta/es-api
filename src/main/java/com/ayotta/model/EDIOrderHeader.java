package com.ayotta.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.springframework.data.elasticsearch.annotations.Document;

@Entity
@Table(name="XMP_EDI_ORDER_HEADERS")
public class EDIOrderHeader {
	
	
	private String elasticSearchRef;
	
	private String orderSource = "EDI";
	
	private String orderType;
	private String messageStatus;
	
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_header_seq")
//	@SequenceGenerator(sequenceName = "order_header_seq", allocationSize = 1,name = "ORDER_HEADER_ID_S")
	@Id
	@SequenceGenerator(name="order_header_gen", sequenceName="order_header_seq",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="order_header_gen")
	@Column(name="ORDER_HEADER_ID")
	private Long orderHeaderId ; 
	private String customerName ; 
	
	private String customerRef ; 
	private String custPoNo ; 
	private Date reqDate ; 
	private String orderCurrency ; 
	private String customerCategory ; 
	private String shipToCode ; 
	private String orderRemarks ; 
	private String site ; 
	private String status ; 
	private String statusMessage ; 
	private String orderLimitAuthorised ; 
	private String emailAddress ; 
	private String cscMilitaryFlag ; 
	private String supplierCode ; 
	private String creditHoldAuthorised ; 
	private Date creationDate ; 
	private String headerHold ; 
	private Date lastUpdateDate ;
	private Date orderTransferDate ;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "orderHeaderId")
	private Set<EDIOrderLine> orderLines = new HashSet<EDIOrderLine>();
	
	
	
	
	public String getElasticSearchRef() {
		return elasticSearchRef;
	}




	public void setElasticSearchRef(String elasticSearchRef) {
		this.elasticSearchRef = elasticSearchRef;
	}




	public String getOrderSource() {
		return orderSource;
	}




	public void setOrderSource(String orderSource) {
		this.orderSource = orderSource;
	}




	public String getOrderType() {
		return orderType;
	}




	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}




	public String getMessageStatus() {
		return messageStatus;
	}




	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}




	public Long getOrderHeaderId() {
		return orderHeaderId;
	}




	public void setOrderHeaderId(Long orderHeaderId) {
		this.orderHeaderId = orderHeaderId;
	}




	public String getCustomerName() {
		return customerName;
	}




	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}




	public String getCustomerRef() {
		return customerRef;
	}




	public void setCustomerRef(String customerRef) {
		this.customerRef = customerRef;
	}




	public String getCustPoNo() {
		return custPoNo;
	}




	public void setCustPoNo(String custPoNo) {
		this.custPoNo = custPoNo;
	}




	public Date getReqDate() {
		return reqDate;
	}




	public void setReqDate(Date reqDate) {
		this.reqDate = reqDate;
	}




	public String getOrderCurrency() {
		return orderCurrency;
	}




	public void setOrderCurrency(String orderCurrency) {
		this.orderCurrency = orderCurrency;
	}




	public String getCustomerCategory() {
		return customerCategory;
	}




	public void setCustomerCategory(String customerCategory) {
		this.customerCategory = customerCategory;
	}




	public String getShipToCode() {
		return shipToCode;
	}




	public void setShipToCode(String shipToCode) {
		this.shipToCode = shipToCode;
	}




	public String getOrderRemarks() {
		return orderRemarks;
	}




	public void setOrderRemarks(String orderRemarks) {
		this.orderRemarks = orderRemarks;
	}




	public String getSite() {
		return site;
	}




	public void setSite(String site) {
		this.site = site;
	}




	public String getStatus() {
		return status;
	}




	public void setStatus(String status) {
		this.status = status;
	}




	public String getStatusMessage() {
		return statusMessage;
	}




	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}




	public String getOrderLimitAuthorised() {
		return orderLimitAuthorised;
	}




	public void setOrderLimitAuthorised(String orderLimitAuthorised) {
		this.orderLimitAuthorised = orderLimitAuthorised;
	}




	public String getEmailAddress() {
		return emailAddress;
	}




	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}




	public String getCscMilitaryFlag() {
		return cscMilitaryFlag;
	}




	public void setCscMilitaryFlag(String cscMilitaryFlag) {
		this.cscMilitaryFlag = cscMilitaryFlag;
	}




	public String getSupplierCode() {
		return supplierCode;
	}




	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}




	public String getCreditHoldAuthorised() {
		return creditHoldAuthorised;
	}




	public void setCreditHoldAuthorised(String creditHoldAuthorised) {
		this.creditHoldAuthorised = creditHoldAuthorised;
	}




	public Date getCreationDate() {
		return creationDate;
	}




	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}




	public String getHeaderHold() {
		return headerHold;
	}




	public void setHeaderHold(String headerHold) {
		this.headerHold = headerHold;
	}




	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}




	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}




	public Date getOrderTransferDate() {
		return orderTransferDate;
	}




	public void setOrderTransferDate(Date orderTransferDate) {
		this.orderTransferDate = orderTransferDate;
	}




	public Set<EDIOrderLine> getOrderLines() {
		return orderLines;
	}




	public void setOrderLines(Set<EDIOrderLine> orderLines) {
		this.orderLines = orderLines;
	}




	@Override
	public String toString() {
		return " {elasticSearchRef=" + elasticSearchRef + ", orderType=" + orderType + ", messageStatus=" + messageStatus + ", orderHeaderId="
				+ orderHeaderId + ", customerName=" + customerName + ", customerRef=" + customerRef + ", custPoNo="
				+ custPoNo + ", reqDate=" + reqDate + ", orderCurrency=" + orderCurrency + ", customerCategory="
				+ customerCategory + ", shipToCode=" + shipToCode + ", orderRemarks=" + orderRemarks + ", site=" + site
				+ ", status=" + status + ", statusMessage=" + statusMessage + ", orderLimitAuthorised="
				+ orderLimitAuthorised + ", emailAddress=" + emailAddress + ", cscMilitaryFlag=" + cscMilitaryFlag
				+ ", supplierCode=" + supplierCode + ", creditHoldAuthorised=" + creditHoldAuthorised
				+ ", creationDate=" + creationDate + ", headerHold=" + headerHold + ", lastUpdateDate=" + lastUpdateDate
				+ ", orderTransferDate=" + orderTransferDate + ", orderLines=" + orderLines + "}";
	} 
	
	
}
