package com.ayotta.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.springframework.data.annotation.Id;

@XmlType(name = "ETAFeed")
@XmlRootElement
public class ETAFeed implements Serializable{
	private static final long serialVersionUID = -8429840615140031156L;
	
	@Id
	private String elasticSearchRef;
	private String messageStatus;
	private String fileName;
	private Date fileDate;
	private Long fileSize;
	private Integer fileDataSize;
	
	private String feedTable;
	private String feedRecord;
	private String feedDate;
	private Long feedId;
	//private String feed;
	private Timestamp creationDate;
	private String sapFeedReference;
	private Timestamp elkCreationDate;
	private Timestamp elkModifiedDate;
	private String orderSource;
	
	
	public ETAFeed() {
	}
	public String getFeedTable() {
		return feedTable;
	}
	public void setFeedTable(String feedTable) {
		this.feedTable = feedTable;
	}
	public String getFeedRecord() {
		return feedRecord;
	}
	public void setFeedRecord(String feedRecord) {
		this.feedRecord = feedRecord;
	}
	public String getFeedDate() {
		return feedDate;
	}
	public void setFeedDate(String feedDate) {
		this.feedDate = feedDate;
	}
	public Long getFeedId() {
		return feedId;
	}
	public void setFeedId(Long feedId) {
		this.feedId = feedId;
	}
	/*public String getFeed() {
		return feed;
	}
	public void setFeed(String feed) {
		this.feed = feed;
	}*/
	public String getElasticSearchRef() {
		return elasticSearchRef;
	}
	public void setElasticSearchRef(String elasticSearchRef) {
		this.elasticSearchRef = elasticSearchRef;
	}
	public String getMessageStatus() {
		return messageStatus;
	}
	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Date getFileDate() {
		return fileDate;
	}
	public void setFileDate(Date fileDate) {
		this.fileDate = fileDate;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Long getFileSize() {
		return fileSize;
	}
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
	public Integer getFileDataSize() {
		return fileDataSize;
	}
	public void setFileDataSize(Integer fileDataSize) {
		this.fileDataSize = fileDataSize;
	}
	public String getSapFeedReference() {
		return sapFeedReference;
	}
	public void setSapFeedReference(String sapFeedReference) {
		this.sapFeedReference = sapFeedReference;
	}
	public Timestamp getElkCreationDate() {
		return elkCreationDate;
	}
	public void setElkCreationDate(Timestamp elkCreationDate) {
		this.elkCreationDate = elkCreationDate;
	}
	public Timestamp getElkModifiedDate() {
		return elkModifiedDate;
	}
	public void setElkModifiedDate(Timestamp elkModifiedDate) {
		this.elkModifiedDate = elkModifiedDate;
	}
	public String getOrderSource() {
		return orderSource;
	}
	public void setOrderSource(String orderSource) {
		this.orderSource = orderSource;
	}
	@Override
	public String toString() {
		return "{elasticSearchRef=" + elasticSearchRef
				+ ", messageStatus=" + messageStatus + ", fileName=" + fileName
				+ ", fileDate=" + fileDate + ", fileSize=" + fileSize
				+ ", fileDataSize=" + fileDataSize + ", feedTable=" + feedTable
				+ ", feedRecord=" + feedRecord + ", feedDate=" + feedDate
				+ ", feedId=" + feedId + ", creationDate=" + creationDate
				+ ", sapFeedReference=" + sapFeedReference
				+ ", elkCreationDate=" + elkCreationDate + ", elkModifiedDate="
				+ elkModifiedDate + "}";
	}
}