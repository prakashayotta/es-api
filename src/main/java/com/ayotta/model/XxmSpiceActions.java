package com.ayotta.model;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="XXM_SPICE_ACTIONS")
public class XxmSpiceActions {
	
	private Integer messageId ;
	private String messageSource ;
	private String messageType ;
	private String customer ;
	private String customerRef ;
	private String cpo ;
	private Timestamp dateReceipt ;
	private String validationError ;
	private String xmlMessage ;
	private Integer validationDecisionPoint ;
	private String origMessage ;
	private String actionMessage ;
	private Integer messageSeq ;
	private String aogFlag ;
	private String actionMsgCode ;
	private String partNo ;
	private String lineNumber ;
	private String returnEmail ;
	private Integer sourceLineId ;
	private String partCategory ;
	private String sourceRowid ;
	private Integer exoEnvid ;
	@Id
	private Long exoHdrid ;
	private Integer exoLineid ;
	private Integer customerId ;
	private String exoChangeCode ;
	private String exoStatus ;
	private Integer exoPocEnvid ;
	private Integer exoPocHdrid ;
	private Integer exoPocLineid ;
	private String solarCpo ;
	private Integer createdFromXorHdrid ;
	private String vendorCode ;
	private Integer legacyCustomerId ;
	public Integer getMessageId() {
		return messageId;
	}
	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}
	public String getMessageSource() {
		return messageSource;
	}
	public void setMessageSource(String messageSource) {
		this.messageSource = messageSource;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getCustomerRef() {
		return customerRef;
	}
	public void setCustomerRef(String customerRef) {
		this.customerRef = customerRef;
	}
	public String getCpo() {
		return cpo;
	}
	public void setCpo(String cpo) {
		this.cpo = cpo;
	}
	public Timestamp getDateReceipt() {
		return dateReceipt;
	}
	public void setDateReceipt(Timestamp dateReceipt) {
		this.dateReceipt = dateReceipt;
	}
	public String getValidationError() {
		return validationError;
	}
	public void setValidationError(String validationError) {
		this.validationError = validationError;
	}
	public String getXmlMessage() {
		return xmlMessage;
	}
	public void setXmlMessage(String xmlMessage) {
		this.xmlMessage = xmlMessage;
	}
	public Integer getValidationDecisionPoint() {
		return validationDecisionPoint;
	}
	public void setValidationDecisionPoint(Integer validationDecisionPoint) {
		this.validationDecisionPoint = validationDecisionPoint;
	}
	public String getOrigMessage() {
		return origMessage;
	}
	public void setOrigMessage(String origMessage) {
		this.origMessage = origMessage;
	}
	public String getActionMessage() {
		return actionMessage;
	}
	public void setActionMessage(String actionMessage) {
		this.actionMessage = actionMessage;
	}
	public Integer getMessageSeq() {
		return messageSeq;
	}
	public void setMessageSeq(Integer messageSeq) {
		this.messageSeq = messageSeq;
	}
	public String getAogFlag() {
		return aogFlag;
	}
	public void setAogFlag(String aogFlag) {
		this.aogFlag = aogFlag;
	}
	public String getActionMsgCode() {
		return actionMsgCode;
	}
	public void setActionMsgCode(String actionMsgCode) {
		this.actionMsgCode = actionMsgCode;
	}
	public String getPartNo() {
		return partNo;
	}
	public void setPartNo(String partNo) {
		this.partNo = partNo;
	}
	public String getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}
	public String getReturnEmail() {
		return returnEmail;
	}
	public void setReturnEmail(String returnEmail) {
		this.returnEmail = returnEmail;
	}
	public Integer getSourceLineId() {
		return sourceLineId;
	}
	public void setSourceLineId(Integer sourceLineId) {
		this.sourceLineId = sourceLineId;
	}
	public String getPartCategory() {
		return partCategory;
	}
	public void setPartCategory(String partCategory) {
		this.partCategory = partCategory;
	}
	public String getSourceRowid() {
		return sourceRowid;
	}
	public void setSourceRowid(String sourceRowid) {
		this.sourceRowid = sourceRowid;
	}
	public Integer getExoEnvid() {
		return exoEnvid;
	}
	public void setExoEnvid(Integer exoEnvid) {
		this.exoEnvid = exoEnvid;
	}
	public Long getExoHdrid() {
		return exoHdrid;
	}
	public void setExoHdrid(Long exoHdrid) {
		this.exoHdrid = exoHdrid;
	}
	public Integer getExoLineid() {
		return exoLineid;
	}
	public void setExoLineid(Integer exoLineid) {
		this.exoLineid = exoLineid;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getExoChangeCode() {
		return exoChangeCode;
	}
	public void setExoChangeCode(String exoChangeCode) {
		this.exoChangeCode = exoChangeCode;
	}
	public String getExoStatus() {
		return exoStatus;
	}
	public void setExoStatus(String exoStatus) {
		this.exoStatus = exoStatus;
	}
	public Integer getExoPocEnvid() {
		return exoPocEnvid;
	}
	public void setExoPocEnvid(Integer exoPocEnvid) {
		this.exoPocEnvid = exoPocEnvid;
	}
	public Integer getExoPocHdrid() {
		return exoPocHdrid;
	}
	public void setExoPocHdrid(Integer exoPocHdrid) {
		this.exoPocHdrid = exoPocHdrid;
	}
	public Integer getExoPocLineid() {
		return exoPocLineid;
	}
	public void setExoPocLineid(Integer exoPocLineid) {
		this.exoPocLineid = exoPocLineid;
	}
	public String getSolarCpo() {
		return solarCpo;
	}
	public void setSolarCpo(String solarCpo) {
		this.solarCpo = solarCpo;
	}
	public Integer getCreatedFromXorHdrid() {
		return createdFromXorHdrid;
	}
	public void setCreatedFromXorHdrid(Integer createdFromXorHdrid) {
		this.createdFromXorHdrid = createdFromXorHdrid;
	}
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	public Integer getLegacyCustomerId() {
		return legacyCustomerId;
	}
	public void setLegacyCustomerId(Integer legacyCustomerId) {
		this.legacyCustomerId = legacyCustomerId;
	}
	@Override
	public String toString() {
		return "XxmSpiceActions [messageId=" + messageId + ", messageSource="
				+ messageSource + ", messageType=" + messageType
				+ ", customer=" + customer + ", customerRef=" + customerRef
				+ ", cpo=" + cpo + ", validationError=" + validationError
				+ ", xmlMessage=" + xmlMessage + ", validationDecisionPoint="
				+ validationDecisionPoint + ", origMessage=" + origMessage
				+ ", actionMessage=" + actionMessage + ", messageSeq="
				+ messageSeq + ", aogFlag=" + aogFlag + ", actionMsgCode="
				+ actionMsgCode + ", partNo=" + partNo + ", lineNumber="
				+ lineNumber + ", returnEmail=" + returnEmail
				+ ", sourceLineId=" + sourceLineId + ", partCategory="
				+ partCategory + ", sourceRowid=" + sourceRowid + ", exoEnvid="
				+ exoEnvid + ", exoHdrid=" + exoHdrid + ", exoLineid="
				+ exoLineid + ", customerId=" + customerId + ", exoChangeCode="
				+ exoChangeCode + ", exoStatus=" + exoStatus + ", exoPocEnvid="
				+ exoPocEnvid + ", exoPocHdrid=" + exoPocHdrid
				+ ", exoPocLineid=" + exoPocLineid + ", solarCpo=" + solarCpo
				+ ", createdFromXorHdrid=" + createdFromXorHdrid
				+ ", vendorCode=" + vendorCode + ", legacyCustomerId="
				+ legacyCustomerId + "]";
	}	

}
