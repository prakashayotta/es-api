package com.ayotta.model;

import java.util.ArrayList;
import java.util.List;

public class EDIOrder {

	List<EDIOrderHeader> ediOrders = new ArrayList<>();
	
	private Integer totalSearchRecord;

	public List<EDIOrderHeader> getEdiOrders() {
		return ediOrders;
	}

	public void setEdiOrders(List<EDIOrderHeader> ediOrders) {
		this.ediOrders = ediOrders;
	}

	public Integer getTotalSearchRecord() {
		return totalSearchRecord;
	}

	public void setTotalSearchRecord(Integer totalSearchRecord) {
		this.totalSearchRecord = totalSearchRecord;
	}

	

}
