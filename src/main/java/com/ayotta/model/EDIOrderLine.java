package com.ayotta.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="XMP_EDI_ORDER_LINES")
public class EDIOrderLine {
	
	
	private String messageStatus;
	
//	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_line_seq")
//    @SequenceGenerator(sequenceName = "order_line_seq", allocationSize = 50, name = "ORDER_LINE_ID_S")
	@Id
	@SequenceGenerator(name="order_line_gen", sequenceName="order_line_seq",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="order_line_gen")
	@Column(name="ORDER_LINE_ID")
	private Long orderLineId ; 
	 
	private String custPoNo ; 
	private String custPoLineNo ; 
	private String partNo ; 
	private Long ordQty ; 
	private String ordUom ; 
	private Double ordUnp ; 
	private Date reqDate ; 
	private String initProv ; 
	private String specialRules ; 
	private String priority ; 
	private String aircraftReg ; 
	private String actionCode ; 
	private String acceptBefore ; 
	private String orderRemarks ; 
	private String quoteNo ; 
	private String leaseInd ; 
	private String discount ; 
	private String status ; 
	private String statusMessage ; 
	private Date lastUpdateDate ; 
	private String lastUpdatedBy ; 
	private String lineLimitAuthorised ; 
	private String qtnAuthorised ; 
	private String ipAuthorised ; 
	private String sprAuthorised ; 
	private String focAuthorised ; 
	private String aogAuthorised ; 
	private Long alternativePartId ; 
	private String cat7Authorised ; 
	private String leaseAuthorised ; 
	private Long shipFromOrgId ; 
	private String applyFreightCharge ; 
	private String customerRepAcknowledged ; 
	private String ataPartSwap ; 
	private String natoPartSwap ; 
	private String custXrefSwap ; 
	private String custPartNo ; 
	private String shipFromStorageLoc ; 
	private String shipFromPlant ; 
	private String contractNumber ; 
	private String cscBlockRelease ; 
	private String embargoAuthorised ; 
	private String standardContract ; 
	private String fleetAuthorised ; 
	private Long freightChargeAmount ; 
	private String qtyApproved ; 
	private Long originalOrderedQty ; 
	private String quantityAmendedFlag ; 
	private String lineHold ; 
	private String shipToCode ; 
	private String delegatedEnduserRef ; 
	private String origDelegatedEnduserRef ; 
	private String cscBlockRelease1 ;
	private Long orderHeaderId;
	public Long getOrderHeaderId() {
		return orderHeaderId;
	}
	public void setOrderHeaderId(Long orderHeaderId) {
		this.orderHeaderId = orderHeaderId;
	}
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "ORDER_HEADER_ID",referencedColumnName="ORDER_HEADER_ID",columnDefinition="orderHeaderId", nullable = false)
//	private EDIOrderHeader orderHeader;
	

//	public EDIOrderHeader getOrderHeader() {
//		return orderHeader;
//	}
//	public void setOrderHeader(EDIOrderHeader orderHeader) {
//		this.orderHeader = orderHeader;
//	}
	public Long getOrderLineId() {
		return orderLineId;
	}
	public void setOrderLineId(Long orderLineId) {
		this.orderLineId = orderLineId;
	}
	
	public String getCustPoNo() {
		return custPoNo;
	}
	public void setCustPoNo(String custPoNo) {
		this.custPoNo = custPoNo;
	}
	public String getCustPoLineNo() {
		return custPoLineNo;
	}
	public void setCustPoLineNo(String custPoLineNo) {
		this.custPoLineNo = custPoLineNo;
	}
	public String getPartNo() {
		return partNo;
	}
	public void setPartNo(String partNo) {
		this.partNo = partNo;
	}
	public Long getOrdQty() {
		return ordQty;
	}
	public void setOrdQty(Long ordQty) {
		this.ordQty = ordQty;
	}
	public String getOrdUom() {
		return ordUom;
	}
	public void setOrdUom(String ordUom) {
		this.ordUom = ordUom;
	}
	public Double getOrdUnp() {
		return ordUnp;
	}
	public void setOrdUnp(Double ordUnp) {
		this.ordUnp = ordUnp;
	}
	public Date getReqDate() {
		return reqDate;
	}
	public void setReqDate(Date reqDate) {
		this.reqDate = reqDate;
	}
	public String getInitProv() {
		return initProv;
	}
	public void setInitProv(String initProv) {
		this.initProv = initProv;
	}
	public String getSpecialRules() {
		return specialRules;
	}
	public void setSpecialRules(String specialRules) {
		this.specialRules = specialRules;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getAircraftReg() {
		return aircraftReg;
	}
	public void setAircraftReg(String aircraftReg) {
		this.aircraftReg = aircraftReg;
	}
	public String getActionCode() {
		return actionCode;
	}
	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}
	public String getAcceptBefore() {
		return acceptBefore;
	}
	public void setAcceptBefore(String acceptBefore) {
		this.acceptBefore = acceptBefore;
	}
	public String getOrderRemarks() {
		return orderRemarks;
	}
	public void setOrderRemarks(String orderRemarks) {
		this.orderRemarks = orderRemarks;
	}
	public String getQuoteNo() {
		return quoteNo;
	}
	public void setQuoteNo(String quoteNo) {
		this.quoteNo = quoteNo;
	}
	public String getLeaseInd() {
		return leaseInd;
	}
	public void setLeaseInd(String leaseInd) {
		this.leaseInd = leaseInd;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public String getLineLimitAuthorised() {
		return lineLimitAuthorised;
	}
	public void setLineLimitAuthorised(String lineLimitAuthorised) {
		this.lineLimitAuthorised = lineLimitAuthorised;
	}
	public String getQtnAuthorised() {
		return qtnAuthorised;
	}
	public void setQtnAuthorised(String qtnAuthorised) {
		this.qtnAuthorised = qtnAuthorised;
	}
	public String getIpAuthorised() {
		return ipAuthorised;
	}
	public void setIpAuthorised(String ipAuthorised) {
		this.ipAuthorised = ipAuthorised;
	}
	public String getSprAuthorised() {
		return sprAuthorised;
	}
	public void setSprAuthorised(String sprAuthorised) {
		this.sprAuthorised = sprAuthorised;
	}
	public String getFocAuthorised() {
		return focAuthorised;
	}
	public void setFocAuthorised(String focAuthorised) {
		this.focAuthorised = focAuthorised;
	}
	public String getAogAuthorised() {
		return aogAuthorised;
	}
	public void setAogAuthorised(String aogAuthorised) {
		this.aogAuthorised = aogAuthorised;
	}
	public Long getAlternativePartId() {
		return alternativePartId;
	}
	public void setAlternativePartId(Long alternativePartId) {
		this.alternativePartId = alternativePartId;
	}
	public String getCat7Authorised() {
		return cat7Authorised;
	}
	public void setCat7Authorised(String cat7Authorised) {
		this.cat7Authorised = cat7Authorised;
	}
	public String getLeaseAuthorised() {
		return leaseAuthorised;
	}
	public void setLeaseAuthorised(String leaseAuthorised) {
		this.leaseAuthorised = leaseAuthorised;
	}
	public Long getShipFromOrgId() {
		return shipFromOrgId;
	}
	public void setShipFromOrgId(Long shipFromOrgId) {
		this.shipFromOrgId = shipFromOrgId;
	}
	public String getApplyFreightCharge() {
		return applyFreightCharge;
	}
	public void setApplyFreightCharge(String applyFreightCharge) {
		this.applyFreightCharge = applyFreightCharge;
	}
	public String getCustomerRepAcknowledged() {
		return customerRepAcknowledged;
	}
	public void setCustomerRepAcknowledged(String customerRepAcknowledged) {
		this.customerRepAcknowledged = customerRepAcknowledged;
	}
	public String getAtaPartSwap() {
		return ataPartSwap;
	}
	public void setAtaPartSwap(String ataPartSwap) {
		this.ataPartSwap = ataPartSwap;
	}
	public String getNatoPartSwap() {
		return natoPartSwap;
	}
	public void setNatoPartSwap(String natoPartSwap) {
		this.natoPartSwap = natoPartSwap;
	}
	public String getCustXrefSwap() {
		return custXrefSwap;
	}
	public void setCustXrefSwap(String custXrefSwap) {
		this.custXrefSwap = custXrefSwap;
	}
	public String getCustPartNo() {
		return custPartNo;
	}
	public void setCustPartNo(String custPartNo) {
		this.custPartNo = custPartNo;
	}
	public String getShipFromStorageLoc() {
		return shipFromStorageLoc;
	}
	public void setShipFromStorageLoc(String shipFromStorageLoc) {
		this.shipFromStorageLoc = shipFromStorageLoc;
	}
	public String getShipFromPlant() {
		return shipFromPlant;
	}
	public void setShipFromPlant(String shipFromPlant) {
		this.shipFromPlant = shipFromPlant;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getCscBlockRelease() {
		return cscBlockRelease;
	}
	public void setCscBlockRelease(String cscBlockRelease) {
		this.cscBlockRelease = cscBlockRelease;
	}
	public String getEmbargoAuthorised() {
		return embargoAuthorised;
	}
	public void setEmbargoAuthorised(String embargoAuthorised) {
		this.embargoAuthorised = embargoAuthorised;
	}
	public String getStandardContract() {
		return standardContract;
	}
	public void setStandardContract(String standardContract) {
		this.standardContract = standardContract;
	}
	public String getFleetAuthorised() {
		return fleetAuthorised;
	}
	public void setFleetAuthorised(String fleetAuthorised) {
		this.fleetAuthorised = fleetAuthorised;
	}
	public Long getFreightChargeAmount() {
		return freightChargeAmount;
	}
	public void setFreightChargeAmount(Long freightChargeAmount) {
		this.freightChargeAmount = freightChargeAmount;
	}
	public String getQtyApproved() {
		return qtyApproved;
	}
	public void setQtyApproved(String qtyApproved) {
		this.qtyApproved = qtyApproved;
	}
	public Long getOriginalOrderedQty() {
		return originalOrderedQty;
	}
	public void setOriginalOrderedQty(Long originalOrderedQty) {
		this.originalOrderedQty = originalOrderedQty;
	}
	public String getQuantityAmendedFlag() {
		return quantityAmendedFlag;
	}
	public void setQuantityAmendedFlag(String quantityAmendedFlag) {
		this.quantityAmendedFlag = quantityAmendedFlag;
	}
	public String getLineHold() {
		return lineHold;
	}
	public void setLineHold(String lineHold) {
		this.lineHold = lineHold;
	}
	public String getShipToCode() {
		return shipToCode;
	}
	public void setShipToCode(String shipToCode) {
		this.shipToCode = shipToCode;
	}
	public String getDelegatedEnduserRef() {
		return delegatedEnduserRef;
	}
	public void setDelegatedEnduserRef(String delegatedEnduserRef) {
		this.delegatedEnduserRef = delegatedEnduserRef;
	}
	public String getOrigDelegatedEnduserRef() {
		return origDelegatedEnduserRef;
	}
	public void setOrigDelegatedEnduserRef(String origDelegatedEnduserRef) {
		this.origDelegatedEnduserRef = origDelegatedEnduserRef;
	}
	public String getCscBlockRelease1() {
		return cscBlockRelease1;
	}
	public void setCscBlockRelease1(String cscBlockRelease1) {
		this.cscBlockRelease1 = cscBlockRelease1;
	}
	public String getMessageStatus() {
		return messageStatus;
	}
	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}
	@Override
	public String toString() {
		return "OrderLine {messageStatus=" + messageStatus + ", orderLineId=" + orderLineId
				+ ", custPoNo=" + custPoNo + ", custPoLineNo=" + custPoLineNo
				+ ", partNo=" + partNo + ", ordQty=" + ordQty + ", ordUom=" + ordUom + ", ordUnp=" + ordUnp
				+ ", reqDate=" + reqDate + ", initProv=" + initProv + ", specialRules=" + specialRules + ", priority="
				+ priority + ", aircraftReg=" + aircraftReg + ", actionCode=" + actionCode + ", acceptBefore="
				+ acceptBefore + ", orderRemarks=" + orderRemarks + ", quoteNo=" + quoteNo + ", leaseInd=" + leaseInd
				+ ", discount=" + discount + ", status=" + status + ", statusMessage=" + statusMessage
				+ ", lastUpdateDate=" + lastUpdateDate + ", lastUpdatedBy=" + lastUpdatedBy + ", lineLimitAuthorised="
				+ lineLimitAuthorised + ", qtnAuthorised=" + qtnAuthorised + ", ipAuthorised=" + ipAuthorised
				+ ", sprAuthorised=" + sprAuthorised + ", focAuthorised=" + focAuthorised + ", aogAuthorised="
				+ aogAuthorised + ", alternativePartId=" + alternativePartId + ", cat7Authorised=" + cat7Authorised
				+ ", leaseAuthorised=" + leaseAuthorised + ", shipFromOrgId=" + shipFromOrgId + ", applyFreightCharge="
				+ applyFreightCharge + ", customerRepAcknowledged=" + customerRepAcknowledged + ", ataPartSwap="
				+ ataPartSwap + ", natoPartSwap=" + natoPartSwap + ", custXrefSwap=" + custXrefSwap + ", custPartNo="
				+ custPartNo + ", shipFromStorageLoc=" + shipFromStorageLoc + ", shipFromPlant=" + shipFromPlant
				+ ", contractNumber=" + contractNumber + ", cscBlockRelease=" + cscBlockRelease + ", embargoAuthorised="
				+ embargoAuthorised + ", standardContract=" + standardContract + ", fleetAuthorised=" + fleetAuthorised
				+ ", freightChargeAmount=" + freightChargeAmount + ", qtyApproved=" + qtyApproved
				+ ", originalOrderedQty=" + originalOrderedQty + ", quantityAmendedFlag=" + quantityAmendedFlag
				+ ", lineHold=" + lineHold + ", shipToCode=" + shipToCode + ", delegatedEnduserRef="
				+ delegatedEnduserRef + ", origDelegatedEnduserRef=" + origDelegatedEnduserRef + ", cscBlockRelease1="
				+ cscBlockRelease1 + "}";
	}
	
	
	
		
	
}
