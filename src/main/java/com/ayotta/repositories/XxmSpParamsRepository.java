package com.ayotta.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ayotta.model.XxmSpParams;
import java.lang.Long;


public interface XxmSpParamsRepository extends JpaRepository<XxmSpParams, Long>{

	 List<XxmSpParams> findByParameterNameLike(String name);
}
