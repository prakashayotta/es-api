package com.ayotta.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ayotta.model.EDIOrderLine;
import java.lang.Long;
import java.util.List;


public interface EDIOrderLineRepository extends JpaRepository<EDIOrderLine, Long>{
	
	List<EDIOrderLine> findByOrderHeaderId(Long orderheaderid);

}
