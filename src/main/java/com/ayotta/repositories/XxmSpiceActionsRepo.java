package com.ayotta.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ayotta.model.XxmSpiceActions;

import java.lang.Long;
import java.util.List;

public interface XxmSpiceActionsRepo extends JpaRepository<XxmSpiceActions, Long>{
	
	List<XxmSpiceActions> findByExoHdrid(Long exohdrid);

}