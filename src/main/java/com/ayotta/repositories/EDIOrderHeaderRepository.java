package com.ayotta.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ayotta.model.EDIOrderHeader;
import java.lang.String;
import java.util.List;


public interface EDIOrderHeaderRepository extends JpaRepository<EDIOrderHeader, Long>{
	
	EDIOrderHeader findByElasticSearchRef(String refId);

}
